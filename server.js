const express = require("express")
const app = express();
const mysql = require("mysql");
const path = require("path");

const PORT = 3000;


app.set('views', path.join(__dirname, "views"));
app.set('view engine', 'hbs');  //use handlebars.js for view engine
app.use(express.static('public/images'));
//app.use(express.static('public/js'));


const DB = mysql.createConnection({
    host: '127.0.0.1',
    user: 'andrea',
    password: '',
    database: 'METEO',
    insecureAuth : true
});

DB.connect((err) => {
    if(err) {
        throw err;
    }
    console.log("MySQL connected")
});





/* ROUTE SECTION */
// app.get('/', (req, res) => {

//     res.render('index');

// });


app.get('/', (req, res) => {
    let query = "select * FROM Station WHERE dateAndTime = (SELECT max(dateAndTime) FROM Station);"
    DB.query(query, (err, result) => {
        if(err) {
            throw err;
        }
        console.log("result: ", result);

        dateAndTime = result[0].dateAndTime;
        dataOdierna = result[0].DataOdierna;
        ora = result[0].Ora;
        temperatura = result[0].Temperatura;
        umidita = result[0].Umidita;
        pressione = result[0].Pressione;

        res.render("test", {
            "dateAndTime": dateAndTime,
            "dataOdierna": dataOdierna,
            "ora": ora,
            "temperatura": temperatura,
            "umidita": umidita,
            "pressione": pressione,
        });

    });
});

app.get('/graph/temp', takeDataForTempGraphic);

function takeDataForTempGraphic(req, res) {
    let query = "SELECT Temperatura, Ora FROM Station WHERE DataOdierna = (SELECT max(DataOdierna) FROM Station);";
    DB.query(query, (err, result) => {
        if(err) {
            throw err;
        }
        console.log("Result: ", result);

        let listOfItems = [];

        for(var i = 0; i < result.length; i++) {
            console.log("dato numero ", i + 1, ": ", result[i]);
            listOfItems.push(i);
        }

        console.log(listOfItems.length);

        let secondaQuery = "SELECT min(Temperatura) as minTemp, max(Temperatura) as maxTemp FROM Station WHERE DataOdierna = (SELECT MAX(DataOdierna) FROM Station);"

        DB.query(secondaQuery, (err, secRes) => {

            if(err) {
                throw err;
            }

            console.log("Secondo result: ", secRes[0]);

            res.render('graphicTemp', {
                'result': result,
                'secRes': secRes,
                'lengthOfIDs': listOfItems.length,
            });
        })

        
    });
};


app.get('/graph/humid', (req, res) => {
    let query = "SELECT Umidita, Ora FROM Station WHERE DataOdierna = (SELECT MAX(DataOdierna) FROM Station);"
    DB.query(query, (err, result) => {
        if(err) {
            throw err;
        }

        console.log("Result: ", result);

        let listOfItems = [];

        for(var i = 0; i < result.length; i++) {
            console.log("dato numero ", i + 1, ": ", result[i]);
            listOfItems.push(i);
        }

        console.log(result.length);

        res.render('graphicHumidity', {
            'result': result,
            'lengthOfIDs': listOfItems.length,
        })

    });
});

app.get('/graph/pression', (req, res) => {
    //take pressure data where DataOdierna is equals to max possible data (the last data in table Station)
    let query = "SELECT Pressione, Ora FROM Station WHERE DataOdierna = (SELECT MAX(DataOdierna) FROM Station); "
    DB.query(query, (err, result) => {
        if(err) {
            throw err;
        }

        console.log("result: ", result);

        //let's get the min and max for pressure
        let secondQuery = "SELECT min(Pressione) as minPress, max(Pressione) as maxPress FROM Station;"

        DB.query(secondQuery, (err, secondResult) => {

            if(err) {
                throw err;
            }

            console.log("secondResult: ", secondResult[0]);

            res.render('graphicPression', {
                "result": result,
                'lengthOfIDs': result.length,
                'secondResult': secondResult,   //max and min values
            });
        });

        


    });
});


app.listen(PORT, () => {
    console.log("running");
})

